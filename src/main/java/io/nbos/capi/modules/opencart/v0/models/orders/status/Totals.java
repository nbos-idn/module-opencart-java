package io.nbos.capi.modules.opencart.v0.models.orders.status;

/**
 * Created by devenv on 11/29/16.
 */

public class Totals {
    private String title;

    private String value;

    private String order_total_id;

    private String sort_order;

    private String code;

    private String order_id;

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    public String getOrder_total_id ()
    {
        return order_total_id;
    }

    public void setOrder_total_id (String order_total_id)
    {
        this.order_total_id = order_total_id;
    }

    public String getSort_order ()
    {
        return sort_order;
    }

    public void setSort_order (String sort_order)
    {
        this.sort_order = sort_order;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getOrder_id ()
    {
        return order_id;
    }

    public void setOrder_id (String order_id)
    {
        this.order_id = order_id;
    }

}
