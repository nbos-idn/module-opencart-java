package io.nbos.capi.modules.opencart.v0.models.cart;

/**
 * Created by vivekkiran on 8/16/16.
 */

public class ConfirmCartResponse {
    private Boolean success;
    private ConfirmCartData data;
    private String error;
    public Boolean getSuccess() {
        return success;
    }

    public ConfirmCartData getData() {
        return data;
    }

    public String getError() {
        return error;
    }


}
