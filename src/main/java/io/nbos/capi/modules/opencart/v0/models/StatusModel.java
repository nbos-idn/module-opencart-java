package io.nbos.capi.modules.opencart.v0.models;

/**
 * Created by devenv on 1/30/17.
 */
public class StatusModel extends ErrorModel {
    private boolean success;

    public ErrorModel getError() {
        return error;
    }

    private ErrorModel error;

    public boolean isSuccess() {
        return success;
    }
}
