package io.nbos.capi.modules.opencart.v0.models.orders;

/**
 * Created by devenv on 11/19/16.
 */

public class OrdersApiModel {
    private DataModel data;

    private Boolean success;

    public DataModel getData ()
    {
        return data;
    }

    public void setData (DataModel data)
    {
        this.data = data;
    }

    public Boolean getSuccess ()
    {
        return success;
    }

    public void setSuccess (Boolean success)
    {
        this.success = success;
    }

}
