package io.nbos.capi.modules.opencart.v0.models.products;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by vivekkiran on 8/6/16.
 */

public class ProductsData extends RealmObject {

    @PrimaryKey
    private int id;


    private String tag;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTag(String tag){
        this.tag = tag;
    }


    public RealmList<OptionsListModel> getOptions() {
        return options;
    }

    public String getName() {
        return name;
    }


    private RealmList<OptionsListModel> options;


    public String getTag() {
        return tag;
    }


}
