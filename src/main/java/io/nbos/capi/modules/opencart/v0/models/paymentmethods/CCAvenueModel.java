package io.nbos.capi.modules.opencart.v0.models.paymentmethods;

/**
 * Created by devenv on 4/25/17.
 */

public class CCAvenueModel {
    String encRequest;
    String access_code;

    public void setEncRequest(String encRequest) {
        this.encRequest = encRequest;
    }

    public void setAccess_code(String access_code) {
        this.access_code = access_code;
    }

}
