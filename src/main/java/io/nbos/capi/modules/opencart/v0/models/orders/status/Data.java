package io.nbos.capi.modules.opencart.v0.models.orders.status;

import java.util.List;

import io.nbos.capi.modules.opencart.v0.models.orders.faq.Faqs;

/**
 * Created by devenv on 11/29/16.
 */

public class Data {

    public String getTotal() {
        return total;
    }

    public String getPayment_postcode() {
        return payment_postcode;
    }

    public String getPayment_address_2() {
        return payment_address_2;
    }

    public String getInvoice_prefix() {
        return invoice_prefix;
    }

    public String getShipping_address_2() {
        return shipping_address_2;
    }

    public String getShipping_address_1() {
        return shipping_address_1;
    }

    public String getPayment_address_1() {
        return payment_address_1;
    }

    public String getLanguage_id() {
        return language_id;
    }

    public String getPayment_zone() {
        return payment_zone;
    }

    public String getPayment_firstname() {
        return payment_firstname;
    }

    public String getCurrency_code() {
        return currency_code;
    }

    public String getStore_name() {
        return store_name;
    }

//    public String[] getRelated_products() {
//        return related_products;
//    }
    public List<Products> getRelated_products() { return related_products; }


    public String getFax() {
        return fax;
    }

    public String getShipping_address_format() {
        return shipping_address_format;
    }

    public String getShipping_city() {
        return shipping_city;
    }

    public String getStore_id() {
        return store_id;
    }

    public String getShipping_country_id() {
        return shipping_country_id;
    }

    public String getDate_time_added() {
        return date_time_added;
    }

    public String[] getVouchers() {
        return vouchers;
    }

    public String getEmail() {
        return email;
    }

    public String getPayment_lastname() {
        return payment_lastname;
    }

    public String getPayment_country() {
        return payment_country;
    }

    public String getInvoice_no() {
        return invoice_no;
    }

    public String getComment() {
        return comment;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public String getShipping_zone() {
        return shipping_zone;
    }

    public String getPayment_zone_id() {
        return payment_zone_id;
    }

    public String getPayment_iso_code_2() {
        return payment_iso_code_2;
    }

    public String getShipping_method() {
        return shipping_method;
    }

    public String getPayment_iso_code_3() {
        return payment_iso_code_3;
    }

    public String getShipping_zone_code() {
        return shipping_zone_code;
    }

    public String getLastname() {
        return lastname;
    }

    public String getOrder_id() {
        return order_id;
    }

    public String getOrder_status_name() {
        return order_status_name;
    }

    public String getPayment_address() {
        return payment_address;
    }

    public String getShipping_firstname() {
        return shipping_firstname;
    }

    public String getShipping_address() {
        return shipping_address;
    }

    public String getPayment_country_id() {
        return payment_country_id;
    }

    public String getDate_added() {
        return date_added;
    }

    public String getPayment_zone_code() {
        return payment_zone_code;
    }

    public List<Products> getProducts() {
        return products;
    }

    public String getShipping_postcode() {
        return shipping_postcode;
    }

    public String getCurrency_id() {
        return currency_id;
    }

    public String getShipping_country() {
        return shipping_country;
    }

    public String getOrder_status_id() {
        return order_status_id;
    }

    public String getStore_url() {
        return store_url;
    }

    public String getDate_modified() {
        return date_modified;
    }

    public String getPayment_company() {
        return payment_company;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getShipping_company() {
        return shipping_company;
    }

    public String getPayment_address_format() {
        return payment_address_format;
    }

    public String getIp() {
        return ip;
    }

    public String getShipping_lastname() {
        return shipping_lastname;
    }

    public String getCurrency_value() {
        return currency_value;
    }

    public String getShipping_zone_id() {
        return shipping_zone_id;
    }

    public String getPayment_city() {
        return payment_city;
    }

    public String getShipping_iso_code_2() {
        return shipping_iso_code_2;
    }

    public String getShipping_iso_code_3() {
        return shipping_iso_code_3;
    }

    public List<Faqs> getFaqs() {
        return faqs;
    }

    public List<Histories> getHistories() {
        return histories;
    }

    public List<Totals> getTotals() {
        return totals;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    private String total;

    private String payment_postcode;

    private String payment_address_2;

    private String invoice_prefix;

    private String shipping_address_2;

    private String shipping_address_1;

    private String payment_address_1;

    private String language_id;

    private String payment_zone;

    private String payment_firstname;

    private String currency_code;

    private String store_name;

//    private String[] related_products;

    private List<Products> related_products;

    private String fax;

    private String shipping_address_format;

    private String shipping_city;

    private String store_id;

    private String shipping_country_id;

    private String date_time_added;

    private String[] vouchers;

    private String email;

    private String payment_lastname;

    private String payment_country;

    private String invoice_no;

    private String comment;

    private String payment_method;

    private String shipping_zone;

    private String payment_zone_id;

    private String payment_iso_code_2;

    private String shipping_method;

    private String payment_iso_code_3;

    private String shipping_zone_code;

    private String lastname;

    private String order_id;

    private String order_status_name;

    private String payment_address;

    private String shipping_firstname;

    private String shipping_address;

    private String payment_country_id;

    private String date_added;

    private String payment_zone_code;

    private List<Products> products;

    private String shipping_postcode;

    private String currency_id;

    private String shipping_country;

    private String order_status_id;

    private String store_url;

    private String date_modified;

    private String payment_company;

    private String firstname;

    private String shipping_company;

    private String payment_address_format;

    private String ip;

    private String shipping_lastname;

    private String currency_value;

    private String shipping_zone_id;

    private String payment_city;

    private String shipping_iso_code_2;

    private String shipping_iso_code_3;

    private List<Faqs> faqs;

    private List<Histories> histories;

    private List<Totals> totals;

    private String telephone;

    private String customer_id;


}
