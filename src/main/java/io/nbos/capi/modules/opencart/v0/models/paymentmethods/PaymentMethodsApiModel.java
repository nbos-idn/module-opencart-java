package io.nbos.capi.modules.opencart.v0.models.paymentmethods;

import io.nbos.capi.modules.opencart.v0.models.common.OcRestMessage;

/**
 * Created by vivekkiran on 8/7/16.
 */

public class PaymentMethodsApiModel  extends OcRestMessage{
    public PaymentsDataModel getData() {
        return data;
    }

    PaymentsDataModel data;
}
