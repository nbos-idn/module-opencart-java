package io.nbos.capi.modules.opencart.v0;


import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

import io.nbos.capi.api.v0.models.TokenApiModel;
import io.nbos.capi.api.v0.support.IdnCallback;
//import io.nbos.capi.api.v0.support.IdsModule;
import io.nbos.capi.api.v0.support.NetworkApi;
import io.nbos.capi.modules.opencart.v0.models.CouponModel;
import io.nbos.capi.modules.opencart.v0.models.StatusModel;
import io.nbos.capi.modules.opencart.v0.models.cart.AddToCartApiModel;
import io.nbos.capi.modules.opencart.v0.models.cart.AddToCartResponse;
import io.nbos.capi.modules.opencart.v0.models.cart.CancelOrderApiModel;
import io.nbos.capi.modules.opencart.v0.models.cart.ConfirmCartResponse;
import io.nbos.capi.modules.opencart.v0.models.cart.DeleteCartApiModel;
import io.nbos.capi.modules.opencart.v0.models.cart.DeleteCartResponse;
import io.nbos.capi.modules.opencart.v0.models.cart.GetCartResponse;
import io.nbos.capi.modules.opencart.v0.models.connect.NbosConnectResponse;
import io.nbos.capi.modules.opencart.v0.models.orders.OrdersApiModel;
import io.nbos.capi.modules.opencart.v0.models.orders.status.OrderStatusApiModel;
import io.nbos.capi.modules.opencart.v0.models.paymentmethods.PaymentMethodBody;
import io.nbos.capi.modules.opencart.v0.models.paymentmethods.SetPaymentResponse;
import io.nbos.capi.modules.opencart.v0.models.products.ProductsApiModel;
import io.nbos.capi.modules.opencart.v0.models.token.NbosConnectModel;
import io.nbos.capi.modules.opencart.v0.utils.OCConstants;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by vivekkiran on 7/29/16.
 */
//@IdsModule(name = "opencart", type = OpenCartApi.class)
public class OpenCartApi extends NetworkApi {





    public OpenCartApi() {
        super();
        setModuleName("opencart");
        setRemoteApiClass(OCRemoteApi.class);
    }

    public void getGuestToken(String authorization) {

        OCRemoteApi ocRemoteApi = getRemoteApi();
//        Observable<TokenApiModel> call = ocRemoteApi.getGuestToken("Basic " + authorization);

        Observable<TokenApiModel> call = ocRemoteApi.getGuestToken("Basic " + authorization, "client_credentials");



        System.out.println("call " + call);

        call.subscribeOn(Schedulers.io()).observeOn(Schedulers.newThread()).subscribe(new Observer<TokenApiModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(TokenApiModel tokenApiModel) {
                apiContext.setUserToken("opencart", tokenApiModel);
                OCConstants.OC_TOKEN = tokenApiModel.access_token;
            }


            @Override
            public void onError(Throwable e) {
                System.out.println("error " + e);
            }

            @Override
            public void onComplete() {

            }

        });
    }

    public NbosConnectResponse connectToNbos(final IdnCallback<NbosConnectResponse> callback) {
        OCRemoteApi ocRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel;

        String ocToken = "";

        if (apiContext.getUserToken("identity") != null) {
            tokenApiModel = apiContext.getUserToken("identity");
        } else {
            tokenApiModel = apiContext.getClientToken();
        }
        NbosConnectModel nbosConnectModel = new NbosConnectModel();
        nbosConnectModel.setNbos_token(tokenApiModel.getAccess_token());
        if (apiContext.getUserToken("opencart") != null) {
            tokenApiModel = apiContext.getUserToken("opencart");

            String tokenValue = tokenApiModel.access_token;

            if(tokenValue.contains("-")){
                ocToken = OCConstants.OC_TOKEN;
            }else {
                ocToken = tokenApiModel.getAccess_token();
            }

        }
        Call<NbosConnectResponse> call = ocRemoteApi.connectToNbos("Bearer " + ocToken, nbosConnectModel);

        NbosConnectResponse connectApiModel = null;
        call.enqueue(new Callback<NbosConnectResponse>() {
            @Override
            public void onResponse(Call<NbosConnectResponse> call, Response<NbosConnectResponse> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                } else {
                    System.out.println(response);
                }
            }

            @Override
            public void onFailure(Call<NbosConnectResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
        return connectApiModel;
    }

    public SetPaymentResponse setPaymentMethod(PaymentMethodBody paymentMethodBody, final IdnCallback<SetPaymentResponse> callback) {
        OCRemoteApi ocRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel;
        if (apiContext.getUserToken("opencart") != null) {
            tokenApiModel = apiContext.getUserToken("opencart");
        } else {
            tokenApiModel = apiContext.getClientToken();
        }
        Call<SetPaymentResponse> call = ocRemoteApi.setPaymentMethod("Bearer " + tokenApiModel.getAccess_token(), paymentMethodBody);

        SetPaymentResponse setPaymentResponse = null;
        call.enqueue(new Callback<SetPaymentResponse>() {
            @Override
            public void onResponse(Call<SetPaymentResponse> call, Response<SetPaymentResponse> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                } else {
                    System.out.println(response);
                }
            }

            @Override
            public void onFailure(Call<SetPaymentResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
        return setPaymentResponse;
    }


    public ProductsApiModel getProductsById(String merchantLanguage, String productId, final IdnCallback<ProductsApiModel> callback) {
        OCRemoteApi ocRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel;
        if (apiContext.getUserToken("opencart") != null) {
            tokenApiModel = apiContext.getUserToken("opencart");
        } else {
            tokenApiModel = apiContext.getClientToken();
        }
        String accessToken = "";

        if (tokenApiModel != null) {
            accessToken = tokenApiModel.getAccess_token();
        }

        System.out.println("accessToken" + accessToken);

        Call<ProductsApiModel> call = ocRemoteApi.getProductsById("Bearer " + accessToken, merchantLanguage, productId);

        ProductsApiModel products = null;
        call.enqueue(new Callback<ProductsApiModel>() {
            @Override
            public void onResponse(Call<ProductsApiModel> call, Response<ProductsApiModel> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                } else {
                    System.out.println(response);
                }
            }

            @Override
            public void onFailure(Call<ProductsApiModel> call, Throwable t) {
                callback.onFailure(t);
            }
        });
        return products;
    }

    public AddToCartResponse addToCart(String currency, AddToCartApiModel addToCartApiModel, final IdnCallback<AddToCartResponse> callback) {
        OCRemoteApi ocRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel = apiContext.getClientToken();
        if (apiContext.getUserToken("opencart") != null) {
            tokenApiModel = apiContext.getUserToken("opencart");
        }
        Call<AddToCartResponse> call = ocRemoteApi.addToCart("Bearer " + tokenApiModel.getAccess_token(), currency, addToCartApiModel);

        AddToCartResponse addToCartResponse = null;
        call.enqueue(new Callback<AddToCartResponse>() {
            @Override
            public void onResponse(Call<AddToCartResponse> call, Response<AddToCartResponse> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                } else {
                    System.out.println(response);
                }
            }

            @Override
            public void onFailure(Call<AddToCartResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
        return addToCartResponse;
    }


    public OrdersApiModel getOrders(final IdnCallback<OrdersApiModel> callback) {
        OCRemoteApi ocRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel = apiContext.getClientToken();
        if (apiContext.getUserToken("opencart") != null) {
            tokenApiModel = apiContext.getUserToken("opencart");
        }

        Call<OrdersApiModel> call = ocRemoteApi.getOrders("Bearer " + tokenApiModel.getAccess_token());

        OrdersApiModel countries = null;
        call.enqueue(new Callback<OrdersApiModel>() {
            @Override
            public void onResponse(Call<OrdersApiModel> call, Response<OrdersApiModel> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                } else {
                    System.out.println(response);
                }
            }

            @Override
            public void onFailure(Call<OrdersApiModel> call, Throwable t) {
                callback.onFailure(t);
            }
        });
        return countries;
    }

    public OrderStatusApiModel getOrderStatus(String orderId, final IdnCallback<OrderStatusApiModel> callback) {
        OCRemoteApi ocRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel = apiContext.getClientToken();
        if (apiContext.getUserToken("opencart") != null) {
            tokenApiModel = apiContext.getUserToken("opencart");
        }

        Call<OrderStatusApiModel> call = ocRemoteApi.getOrderStatus("Bearer " + tokenApiModel.getAccess_token(), orderId);

        OrderStatusApiModel countries = null;
        call.enqueue(new Callback<OrderStatusApiModel>() {
            @Override
            public void onResponse(Call<OrderStatusApiModel> call, Response<OrderStatusApiModel> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                } else {
                    System.out.println(response);
                }
            }

            @Override
            public void onFailure(Call<OrderStatusApiModel> call, Throwable t) {
                callback.onFailure(t);
            }
        });
        return countries;
    }


    public ResponseBody cancelOrder(String orderId, CancelOrderApiModel cancelOrderApiModel, final IdnCallback<ResponseBody> callback) {

        OCRemoteApi ocRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel = apiContext.getClientToken();
        if (apiContext.getUserToken("opencart") != null) {
            tokenApiModel = apiContext.getUserToken("opencart");
        }

        Call<ResponseBody> call = ocRemoteApi.cancelOrder("Bearer " + tokenApiModel.getAccess_token(), orderId, cancelOrderApiModel);

        ResponseBody cancelOrderResponse = null;
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                } else {
                    System.out.println(response);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callback.onFailure(t);
            }
        });

        return cancelOrderResponse;

    }


    public ConfirmCartResponse confirmCart(String lang, final IdnCallback<ConfirmCartResponse> callback) {
        OCRemoteApi ocRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel = apiContext.getClientToken();
        if (apiContext.getUserToken("opencart") != null) {
            tokenApiModel = apiContext.getUserToken("opencart");
        }

      //  String selLang = CommonPrefrences.getStringValue("lang");
//        openCartApi.getCart(selLang + "-in", new IdnCallback<GetCartResponse>() {


        Call<ConfirmCartResponse> call = ocRemoteApi.confirmCart("Bearer " + tokenApiModel.getAccess_token(), lang);

        ConfirmCartResponse confirmCartResponse = null;
        call.enqueue(new Callback<ConfirmCartResponse>() {
            @Override
            public void onResponse(Call<ConfirmCartResponse> call, Response<ConfirmCartResponse> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                } else {
                    System.out.println(response);
                }
            }

            @Override
            public void onFailure(Call<ConfirmCartResponse> call, Throwable t) {
                callback.onFailure(t);
            }
        });
        return confirmCartResponse;
    }

    public ResponseBody payCart(final IdnCallback<ResponseBody> callback) {
        OCRemoteApi ocRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel = apiContext.getClientToken();
        if (apiContext.getUserToken("opencart") != null) {
            tokenApiModel = apiContext.getUserToken("opencart");
        }
        Call<ResponseBody> call = ocRemoteApi.payCart("Bearer " + tokenApiModel.getAccess_token());

        ResponseBody confirmCartResponse = null;
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                } else {
                    System.out.println(response);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callback.onFailure(t);
            }
        });
        return confirmCartResponse;
    }
    public ResponseBody getCCAvenueContent(String accessCode, String encRequest, final IdnCallback<ResponseBody> callback) {
        OCRemoteApi ocRemoteApi = getRemoteApi();
        Map<String, RequestBody> map = new HashMap<>();
        RequestBody accCode = RequestBody.create(MediaType.parse("text/plain"), accessCode);
        RequestBody encReq = RequestBody.create(MediaType.parse("text/plain"), encRequest);
        map.put("encRequest", encReq);
        map.put("access_code", accCode);
        Call<ResponseBody> call = ocRemoteApi.getCCAvenueContent("https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction",map);
        ResponseBody confirmCartResponse = null;
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                } else {
                    System.out.println(response);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callback.onFailure(t);
            }
        });
        return confirmCartResponse;
    }
    public StatusModel emptyCart(final IdnCallback<StatusModel> callback) {
        OCRemoteApi ocRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel;
        if (apiContext.getUserToken("opencart") != null) {
            tokenApiModel = apiContext.getUserToken("opencart");
        } else {
            tokenApiModel = apiContext.getClientToken();
        }
        Call<StatusModel> call = ocRemoteApi.emptyCart("Bearer " + tokenApiModel.getAccess_token());

        StatusModel statusModel = null;
        call.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                } else {
                    System.out.println(response);
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                callback.onFailure(t);
            }
        });
        return statusModel;
    }

    public StatusModel addCoupon(String coupon, final IdnCallback<StatusModel> callback) {
        OCRemoteApi ocRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel;
        if (apiContext.getUserToken("opencart") != null) {
            tokenApiModel = apiContext.getUserToken("opencart");
        } else {
            tokenApiModel = apiContext.getClientToken();
        }
        CouponModel couponModel = new CouponModel();
        couponModel.setCoupon(coupon);
        Call<StatusModel> call = ocRemoteApi.addCoupon("Bearer " + tokenApiModel.getAccess_token(), couponModel);
        StatusModel statusModel = null;
        call.enqueue(new Callback<StatusModel>() {
            @Override
            public void onResponse(Call<StatusModel> call, Response<StatusModel> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                } else {
                    System.out.println(response);
                }
            }

            @Override
            public void onFailure(Call<StatusModel> call, Throwable t) {
                callback.onFailure(t);
            }
        });
        return statusModel;
    }

    public void getCart(String lang,final IdnCallback<GetCartResponse> callback) {
        OCRemoteApi ocRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel = apiContext.getClientToken();
        if (apiContext.getUserToken("opencart") != null) {
            tokenApiModel = apiContext.getUserToken("opencart");
        }
        Call<GetCartResponse> call = ocRemoteApi.getCart("Bearer " + tokenApiModel.getAccess_token(), "INR",lang);
        call.enqueue(new Callback<GetCartResponse>() {
            @Override
            public void onResponse(Call<GetCartResponse> call, Response<GetCartResponse> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                } else {
                    System.out.println(response);
                }
            }

            @Override
            public void onFailure(Call<GetCartResponse> call, Throwable t) {
                callback.onFailure(t);

            }
        });
    }

    public void deleteProductFromCart(String productId, String productOptionId, String productOptionValueId, final IdnCallback<DeleteCartResponse> callback) {
        OCRemoteApi ocRemoteApi = getRemoteApi();
        TokenApiModel tokenApiModel = apiContext.getClientToken();
        if (apiContext.getUserToken("opencart") != null) {
            tokenApiModel = apiContext.getUserToken("opencart");
        }
        DeleteCartApiModel deleteCartApiModel = new DeleteCartApiModel();
        deleteCartApiModel.setProduct_id(productId);
        deleteCartApiModel.setProduct_option_id(productOptionId);
        deleteCartApiModel.setProduct_option_value_id(productOptionValueId);
        Call<DeleteCartResponse> call = ocRemoteApi.removeFromCart("Bearer " + tokenApiModel.getAccess_token(), deleteCartApiModel);
        call.enqueue(new Callback<DeleteCartResponse>() {
            @Override
            public void onResponse(Call<DeleteCartResponse> call, Response<DeleteCartResponse> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                } else {
                    System.out.println(response);
                }
            }

            @Override
            public void onFailure(Call<DeleteCartResponse> call, Throwable t) {
                callback.onFailure(t);

            }
        });
    }
}
