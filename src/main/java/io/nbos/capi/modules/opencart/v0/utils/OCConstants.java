package io.nbos.capi.modules.opencart.v0.utils;

/**
 * Created by devenv on 3/22/17.
 */

public class OCConstants {
    public static final String nbosConnect = "/index.php?route=rest/nbos/connect";
    public static final String ocToken = "/api/rest/oauth2/token/client_credentials";
    public static final String productById = "/api/rest/products/{product_id}";
    public static final String countriesUrl = "/api/rest/countries";
    public static final String zoneIdUrl = "/api/rest/countries/{country_id}";
    public static final String paymentMethods = "/api/rest/paymentmethods";
    public static final String cartUrl = "/api/rest/cart/";
    public static final String updatePwdUrl = "/api/rest/account/password";
    public static final String loginUrl = "/api/rest/login";
    public static final String forgotUrl = "/api/rest/forgotten";
    public static final String logoutUrl = "/api/rest/logout";
    public static final String registerUrl = "/api/rest/register";
    public static final String profileUrl = "/api/rest/account";
    public static final String couponUrl = "/api/rest/coupon";
    public static final String customerOrdersUrl = "/api/rest/customerorders";
    public static final String customerOrderView = "/api/rest/customerorders/{orderId}";
    public static final String addressUrl = "/api/rest/account/address";
    public static final String couponsUrl = "/api/rest/custom/coupons/";
    public static final String cartConfirm = "/api/rest/confirm";
    public static final String cartPay = "/api/rest/pay";
    public static final String emptyCart = "/api/rest/cart/empty";

    public static final String cancelOrder = "/api/rest/orderhistory/{orderId}";



    public static String OC_TOKEN = "";

}
