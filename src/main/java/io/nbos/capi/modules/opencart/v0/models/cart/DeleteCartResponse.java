package io.nbos.capi.modules.opencart.v0.models.cart;

/**
 * Created by devenv on 3/16/17.
 */

public class DeleteCartResponse {
    public Boolean getSuccess() {
        return success;
    }

    public String getTotal() {
        return total;
    }

    private Boolean success;
    private String total;

}
