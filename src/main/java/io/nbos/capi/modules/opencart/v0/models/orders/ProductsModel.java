package io.nbos.capi.modules.opencart.v0.models.orders;

import java.util.List;

/**
 * Created by devenv on 11/19/16.
 */

public class ProductsModel {
    private String model;

    private String name;

    private String quantity;

    private List<OptionModel> option;

    public String getModel ()
    {
        return model;
    }

    public void setModel (String model)
    {
        this.model = model;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getQuantity ()
    {
        return quantity;
    }

    public void setQuantity (String quantity)
    {
        this.quantity = quantity;
    }

    public List<OptionModel> getOption ()
    {
        return option;
    }

    public void setOption (List<OptionModel> option)
    {
        this.option = option;
    }

}
