package io.nbos.capi.modules.opencart.v0.models.orders;

import java.util.List;

/**
 * Created by devenv on 11/19/16.
 */

public class OrdersModel {
    private String total;

    private String currency_value;

    private String date_added;

    private String status;

    private String name;

    private String order_id;

    private String currency_code;

    private List<ProductsModel> products;

    private String date_time_added;

    public String getTotal ()
    {
        return total;
    }

    public void setTotal (String total)
    {
        this.total = total;
    }

    public String getCurrency_value ()
    {
        return currency_value;
    }

    public void setCurrency_value (String currency_value)
    {
        this.currency_value = currency_value;
    }

    public String getDate_added ()
    {
        return date_added;
    }

    public void setDate_added (String date_added)
    {
        this.date_added = date_added;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getOrder_id ()
    {
        return order_id;
    }

    public void setOrder_id (String order_id)
    {
        this.order_id = order_id;
    }

    public String getCurrency_code ()
    {
        return currency_code;
    }

    public void setCurrency_code (String currency_code)
    {
        this.currency_code = currency_code;
    }

    public List<ProductsModel> getProducts ()
    {
        return products;
    }

    public void setProducts (List<ProductsModel> products)
    {
        this.products = products;
    }

    public String getDate_time_added ()
    {
        return date_time_added;
    }

    public void setDate_time_added (String date_time_added)
    {
        this.date_time_added = date_time_added;
    }
}
