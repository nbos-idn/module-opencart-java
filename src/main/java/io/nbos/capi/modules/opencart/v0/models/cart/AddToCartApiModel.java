package io.nbos.capi.modules.opencart.v0.models.cart;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

/**
 * Created by vivekkiran on 8/7/16.
 */

public class AddToCartApiModel {

    @SerializedName("product_id")
    private String productId;
    private String quantity;
    private Map<String, Object> option;

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public void setOption(Map<String, Object> option) {
        this.option = option;
    }


}
