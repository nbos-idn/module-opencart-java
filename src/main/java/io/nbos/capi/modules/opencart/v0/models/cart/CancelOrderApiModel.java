package io.nbos.capi.modules.opencart.v0.models.cart;

/**
 * Created by Afsara on 27/10/17.
 */

public class CancelOrderApiModel {

    private String order_status_id;
    private String comment;
    private Boolean notify;


    public void setOrderStatusID(String order_status_id) {
        this.order_status_id = order_status_id;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setNotify(Boolean notify) {
        this.notify = notify;
    }

}
