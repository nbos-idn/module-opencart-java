package io.nbos.capi.modules.opencart.v0;

import io.nbos.capi.modules.opencart.v0.models.products.OptionValueListModel;
import io.nbos.capi.modules.opencart.v0.models.products.OptionsListModel;
import io.nbos.capi.modules.opencart.v0.models.products.ProductsData;
import io.realm.annotations.RealmModule;

/**
 * Created by devenv on 12/29/16.
 */
@RealmModule(library = true, classes = {ProductsData.class, OptionsListModel.class, OptionValueListModel.class})
public class OpenCartRealmModule {
}
