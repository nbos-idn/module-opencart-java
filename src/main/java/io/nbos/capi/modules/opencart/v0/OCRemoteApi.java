package io.nbos.capi.modules.opencart.v0;

import java.util.Map;

import io.nbos.capi.api.v0.models.TokenApiModel;
import io.nbos.capi.modules.opencart.v0.models.CouponModel;
import io.nbos.capi.modules.opencart.v0.models.StatusModel;
import io.nbos.capi.modules.opencart.v0.models.cart.AddToCartApiModel;
import io.nbos.capi.modules.opencart.v0.models.cart.AddToCartResponse;
import io.nbos.capi.modules.opencart.v0.models.cart.CancelOrderApiModel;
import io.nbos.capi.modules.opencart.v0.models.cart.ConfirmCartResponse;
import io.nbos.capi.modules.opencart.v0.models.cart.DeleteCartApiModel;
import io.nbos.capi.modules.opencart.v0.models.cart.DeleteCartResponse;
import io.nbos.capi.modules.opencart.v0.models.cart.GetCartResponse;
import io.nbos.capi.modules.opencart.v0.models.connect.NbosConnectResponse;
import io.nbos.capi.modules.opencart.v0.models.orders.OrdersApiModel;
import io.nbos.capi.modules.opencart.v0.models.orders.status.OrderStatusApiModel;
import io.nbos.capi.modules.opencart.v0.models.paymentmethods.PaymentMethodBody;
import io.nbos.capi.modules.opencart.v0.models.paymentmethods.PaymentMethodsApiModel;
import io.nbos.capi.modules.opencart.v0.models.paymentmethods.SetPaymentResponse;
import io.nbos.capi.modules.opencart.v0.models.products.ProductsApiModel;
import io.nbos.capi.modules.opencart.v0.models.token.NbosConnectModel;
import io.nbos.capi.modules.opencart.v0.utils.OCConstants;
import io.reactivex.Observable;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;

/**
 * Created by vivekkiran on 7/29/16.
 */

public interface OCRemoteApi {


    @POST(OCConstants.nbosConnect)
    Call<NbosConnectResponse> connectToNbos(@Header("Authorization") String authorization, @Body NbosConnectModel nbosConnectModel);

    @POST(OCConstants.cartUrl)
    Call<AddToCartResponse> addToCart(@Header("Authorization") String authorization, @Header("X-Oc-Currency") String currency, @Body AddToCartApiModel addToCartApiModel);

    @POST(OCConstants.cartConfirm)
    Call<ConfirmCartResponse> confirmCart(@Header("Authorization") String authorization,@Header("X-Oc-Merchant-Language") String merchantLanguage);

    @POST(OCConstants.cartPay)
    Call<ResponseBody> payCart(@Header("Authorization") String authorization);

    @POST
    @Multipart
    Call<ResponseBody> getCCAvenueContent(@Url String url, @PartMap Map<String, RequestBody> partMap);

    @GET(OCConstants.customerOrdersUrl)
    Call<OrdersApiModel> getOrders(@Header("Authorization") String authorization);

    @GET(OCConstants.customerOrderView)
    Call<OrderStatusApiModel> getOrderStatus(@Header("Authorization") String authorization, @Path("orderId") String orderId);

    @Headers( "Content-Type: application/json" )
    @PUT(OCConstants.cancelOrder)
    Call<ResponseBody> cancelOrder(@Header("Authorization") String authorization, @Path("orderId") String orderId, @Body CancelOrderApiModel cancelOrderApiModel);


    @GET(OCConstants.cartUrl)
    Call<GetCartResponse> getCart(@Header("Authorization") String authorization, @Header("X-Oc-Currency") String currency,@Header("X-Oc-Merchant-Language") String merchantLanguage);


    @HTTP(method = "DELETE", path = OCConstants.cartUrl, hasBody = true)
    Call<DeleteCartResponse> removeFromCart(@Header("Authorization") String authorization, @Body DeleteCartApiModel paymentMethodBody);

    @GET(OCConstants.paymentMethods)
    Call<PaymentMethodsApiModel> getPaymentMethods(@Header("Authorization") String authorization);

    @POST(OCConstants.paymentMethods)
    Call<SetPaymentResponse> setPaymentMethod(@Header("Authorization") String authorization, @Body PaymentMethodBody paymentMethodBody);


    @GET(OCConstants.productById)
    Call<ProductsApiModel> getProductsById(@Header("Authorization") String authorization, @Header("X-Oc-Merchant-Language") String merchantLanguage, @Path("product_id") String productId);



//    @POST(OCConstants.ocToken)
//    Observable<TokenApiModel> getGuestToken(@Header("Authorization") String authorization);


    @FormUrlEncoded
    @POST(OCConstants.ocToken)
    Observable<TokenApiModel> getGuestToken(@Header("Authorization") String authorization, @Field("grant_type") String grantType);



    @DELETE(OCConstants.emptyCart)
    Call<StatusModel> emptyCart(@Header("Authorization") String authorization);

    @POST(OCConstants.couponUrl)
    Call<StatusModel> addCoupon(@Header("Authorization") String authorization, @Body CouponModel couponModel);
}
