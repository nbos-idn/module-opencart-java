package io.nbos.capi.modules.opencart.v0.models.orders.status;

/**
 * Created by devenv on 11/29/16.
 */

public class OrderStatusApiModel {
    private Data data;

    private String success;

    public Data getData ()
    {
        return data;
    }

    public void setData (Data data)
    {
        this.data = data;
    }

    public String getSuccess ()
    {
        return success;
    }

    public void setSuccess (String success)
    {
        this.success = success;
    }

}
