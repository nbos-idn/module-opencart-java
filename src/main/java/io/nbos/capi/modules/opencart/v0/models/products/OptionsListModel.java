package io.nbos.capi.modules.opencart.v0.models.products;


import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by vivekkiran on 8/7/16.
 */

public class OptionsListModel extends RealmObject {


    String name;
    @PrimaryKey
    String product_option_id;
    RealmList<OptionValueListModel> option_value;


    public void setName(String name) {
        this.name = name;
    }

    public void setProduct_option_id(String product_option_id) {
        this.product_option_id = product_option_id;
    }

    public void setOption_value(RealmList<OptionValueListModel> option_value) {
        this.option_value = option_value;
    }


    public String getName() {
        return name;
    }

    public String getProduct_option_id() {
        return product_option_id;
    }


    public RealmList<OptionValueListModel> getOption_value() {
        return option_value;
    }


}
