package io.nbos.capi.modules.opencart.v0.models.orders.status;

/**
 * Created by devenv on 11/29/16.
 */

public class Histories {
    private String status;

    private String date_added;

    private String comment;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getDate_added ()
    {
        return date_added;
    }

    public void setDate_added (String date_added)
    {
        this.date_added = date_added;
    }

    public String getComment ()
    {
        return comment;
    }

    public void setComment (String comment)
    {
        this.comment = comment;
    }
}
