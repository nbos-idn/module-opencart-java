package io.nbos.capi.modules.opencart.v0.models.products;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by vivekkiran on 8/6/16.
 */

public class ProductsApiModel extends RealmObject{

    public Boolean getSuccess() {
        return success;
    }

    public ProductsData getData() {
        return data;
    }

    Boolean success;
    ProductsData data;
}
