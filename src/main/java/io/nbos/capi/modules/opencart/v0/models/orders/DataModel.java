package io.nbos.capi.modules.opencart.v0.models.orders;

import java.util.List;

/**
 * Created by devenv on 11/19/16.
 */

public class DataModel {
    private List<OrdersModel> orders;

    public List<OrdersModel> getOrders ()
    {
        return orders;
    }

    public void setOrders (List<OrdersModel> orders)
    {
        this.orders = orders;
    }

}
