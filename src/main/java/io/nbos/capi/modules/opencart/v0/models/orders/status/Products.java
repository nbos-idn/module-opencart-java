package io.nbos.capi.modules.opencart.v0.models.orders.status;

import java.util.List;

/**
 * Created by devenv on 11/29/16.
 */

public class Products {
    private String total;

    private String model;

    private String price;

    private String name;

    private String quantity;

    private List<Option> option;

    public String getTotal ()
    {
        return total;
    }

    public void setTotal (String total)
    {
        this.total = total;
    }

    public String getModel ()
    {
        return model;
    }

    public void setModel (String model)
    {
        this.model = model;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getQuantity ()
    {
        return quantity;
    }

    public void setQuantity (String quantity)
    {
        this.quantity = quantity;
    }

    public List<Option> getOption ()
    {
        return option;
    }

    public void setOption (List<Option> option)
    {
        this.option = option;
    }
}
