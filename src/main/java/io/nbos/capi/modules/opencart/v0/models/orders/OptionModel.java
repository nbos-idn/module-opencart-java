package io.nbos.capi.modules.opencart.v0.models.orders;

/**
 * Created by devenv on 11/19/16.
 */

public class OptionModel {
    private String name;

    private String value;

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

}
