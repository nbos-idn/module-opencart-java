package io.nbos.capi.modules.opencart.v0.models.orders.faq;


/**
 * Created by devenv on 10/25/16.
 */

public class Faqs {
    private String title;

    private String faq_id;

    private String description;

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    String is_active;

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getFaq_id ()
    {
        return faq_id;
    }

    public void setFaq_id (String faq_id)
    {
        this.faq_id = faq_id;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

}
