package io.nbos.capi.modules.opencart.v0.models.orders.status;

/**
 * Created by devenv on 11/29/16.
 */

public class Option {
    private String ref_doc_url;


    private String pundit_answer;

    private String ref_doc;

    private String pundit_name;

    private String order_option_status_id;

    private String contact_pundit_content;

    private String name;

    private String contact_pundit;

    private String value;

    private String order_option_status_name;

    public String getRef_doc_url ()
    {
        return ref_doc_url;
    }

    public void setRef_doc_url (String ref_doc_url)
    {
        this.ref_doc_url = ref_doc_url;
    }

    public String getPundit_answer ()
    {
        return pundit_answer;
    }

    public void setPundit_answer (String pundit_answer)
    {
        this.pundit_answer = pundit_answer;
    }

    public String getRef_doc ()
    {
        return ref_doc;
    }

    public void setRef_doc (String ref_doc)
    {
        this.ref_doc = ref_doc;
    }

    public String getPundit_name ()
    {
        return pundit_name;
    }

    public void setPundit_name (String pundit_name)
    {
        this.pundit_name = pundit_name;
    }

    public String getOrder_option_status_id ()
    {
        return order_option_status_id;
    }

    public void setOrder_option_status_id (String order_option_status_id)
    {
        this.order_option_status_id = order_option_status_id;
    }

    public String getContact_pundit_content ()
    {
        return contact_pundit_content;
    }

    public void setContact_pundit_content (String contact_pundit_content)
    {
        this.contact_pundit_content = contact_pundit_content;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getContact_pundit ()
    {
        return contact_pundit;
    }

    public void setContact_pundit (String contact_pundit)
    {
        this.contact_pundit = contact_pundit;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    public String getOrder_option_status_name ()
    {
        return order_option_status_name;
    }

    public void setOrder_option_status_name (String order_option_status_name)
    {
        this.order_option_status_name = order_option_status_name;
    }
}
